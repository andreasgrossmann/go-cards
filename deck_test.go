package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	// 4 suits * 4 values
	if len(d) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected first card in deck to be 'Ace of Spades', but got '%v'", d[0])
	}

	if d[len(d)-1] != "Four of Clubs" {
		t.Errorf("Expected last card in deck to be 'Four of Clubs', but got '%v'", d[len(d)-1])
	}
}

func TestSaveToFileAndTestNewDeckFromFile(t *testing.T) {
	os.Remove("_deckTest")

	d := newDeck()
	d.saveToFile("_deckTest")

	loadedDeck := newDeckFromFile("_deckTest")

	// 4 suits * 4 values
	if len(loadedDeck) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(loadedDeck))
	}

	os.Remove("_deckTest")
}
